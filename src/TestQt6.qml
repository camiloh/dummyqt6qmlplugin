import QtQuick
import QtQuick.Controls

ApplicationWindow {
id: root

menuBar: MenuBar
{
    Menu
    {
        title: "text"
        MenuItem
        {
            text: "Whatever"
        }
    }
}

   Rectangle
    {
        color: "pink"
        implicitHeight: 100
        implicitWidth: 200
        anchors.centerIn: parent
    }
}
