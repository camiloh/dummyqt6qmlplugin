import QtQuick
import QtQuick.Controls

ApplicationWindow
{
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    header:ToolBar
    {
        contentItem: Row
        {
        ToolButton
        {
            icon.name: "love"
        }

        ToolButton
        {
            icon.name: "folder"
        }
        }
    }

    menuBar: MenuBar
    {
        Menu
        {
            title: "File"
            MenuItem
            {
                text: "test"
            }
        }
    }


    Switch
    {
        anchors.centerIn: parent
    }
}
